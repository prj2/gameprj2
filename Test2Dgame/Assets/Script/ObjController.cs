using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjController : MonoBehaviour
{

    [SerializeField]
    public float distanceFromPlayer;

    private GameObject player;

    public List<ActivatorItem> activatorItems;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        activatorItems = new List<ActivatorItem>();

        StartCoroutine("CheckActivation");
    }

    IEnumerable CheckActivation()
    {
        List<ActivatorItem> removeList = new List<ActivatorItem>();

        if(activatorItems.Count > 0)
        {
            foreach(ActivatorItem item in activatorItems)
            {
                if(Vector2.Distance(player.transform.position, item.objPos) > distanceFromPlayer)
                {
                    if(item.obj == null)
                    {
                        removeList.Add(item);
                    }
                    else
                    {
                        item.obj.SetActive(false);
                    }
                }
                else
                {
                    if(item.obj == null)
                    {
                        removeList.Add(item);
                    }
                    else
                    {
                        item.obj.SetActive(true);
                    }
                }
            }
        }

        yield return new WaitForSeconds(0.01f);

        if(removeList.Count > 0)
        {
            foreach (ActivatorItem item in removeList)
            {
                activatorItems.Remove(item);
            }
        }

        yield return new WaitForSeconds(0.01f);
        StartCoroutine("CheckActivation");
    }
}

public class ActivatorItem
{
    public GameObject obj;
    public Vector2 objPos;
}