using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableFaraway : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject itemActivatorObject;
    private ObjController activationScript; 

    void Start()
    {
        itemActivatorObject = GameObject.Find("Object Controller");
        activationScript = itemActivatorObject.GetComponent<ObjController>();

        StartCoroutine("AddtoList");
    }

    IEnumerable AddtoList()
    {
        yield return new WaitForSeconds(0.1f);

        activationScript.activatorItems.Add(new ActivatorItem { obj = this.gameObject, objPos = transform.position });
    }

}
